# IDS 721 Individual Proj 2 [![pipeline status](https://gitlab.com/jaxonyue/ids-721-individual-proj-2/badges/main/pipeline.svg)](https://gitlab.com/jaxonyue/ids-721-individual-proj-2/-/commits/main)

## Overview
* This repository includes the components for **Individual Project 2 - Continuous Delivery of Rust Microservice**

## Demo Video
https://youtu.be/nHSm32_X4fc

## Goal
* Simple REST API/web service in Rust
* Dockerfile to containerize service
* CI/CD pipeline files

## My REST API Overview
* I adapted my work from Mini Project 7 to create a REST API that takes in a description of a technology and returns the top 2 most similar technologies based on the description
* I used a dataset called `tech.jsonl` that contains descriptions of various technologies
* My Lambda Function would ingest the dataset into a vector database using Qdrant, perform queries and return visualizations of the result
* I used Docker to containerize the Lambda Function and pushed the image to AWS ECR
* I used Gitlab CI/CD to automate the deployment of the Docker image to AWS ECR and the Lambda Function

## Screenshot of the Lambda Function
![Lambda_Function](/uploads/53b696f16bc424f3a1d4fb393be45728/Lambda_Function.png)

## Screenshot of the Qdrant Collection
![Qdrant](/uploads/72d24c4c599b5c3d5557005ce4155b4c/Qdrant.png)

## Screenshot of the Docker Image in AWS ECR Container Registry
![Docker_Image](/uploads/13ffe927afaea98f9d6f9f0c78a0c180/Docker_Image.png)

## Screenshot of Sample Query and Visualization
My query is "mobile". I called the API using the link below and the returned result is below
https://wqkuzp82v4.execute-api.us-east-1.amazonaws.com/default/mini7?q='mobile'
![Sample_Query](/uploads/bce3ddc785803dfb1b65d71ec5acff3e/Sample_Query.png)

## Key Steps
1. Install Rust and Cargo Lambda per the instructions in the [Cargo Lambda documentation](https://www.cargo-lambda.info/guide/installation.html) and [Rust documentation](https://www.rust-lang.org/tools/install)
2. Sign up for Qdrant, create a new cluster, and take the API key and cluster URL
3. Sign up for Cohere and take the API key
4. Create a new Rust project using `cargo lambda new <project_name>`
5. Store your API keys in a `.env` file
6. Create a `.gitignore` file and add `.env` to it to prevent the file from being pushed to Gitlab
7. Create a `setup.rs` file in the `src` directory and add code to set up the Qdrant data ingestion
8. Add the required dependencies to the `Cargo.toml` file
9. Add the following code to the `Cargo.toml` file to run the data ingestion setup (after completing, comment out this part)
```
[[bin]]
name = "setup_collection"
path = "src/setup.rs"
```
10. Run `cargo run --bin setup_collection tech.jsonl` to set up the Qdrant collection
11. Check the Qdrant dashboard to see if the collection was created
12. Write the code for performing queries and outputing visualizations in the `src/main.rs` file
13. Run `cargo lambda watch` and test locally by going to this URL: `http://localhost:9000/?q='mobile'`
14. Once verifed, go to your AWS account and make sure you have the following policies attached to your user: `IAMFullAccess`, `AWSLambda_FullAccess`, and `AmazonEC2ContainerRegistryFullAccess`
15. Store your AWS credentials, the Qdrant credentials, and Cohere credentials as Gitlab CI/CD environment variables
16. Go to ECR Registry on AWS and create a new private registry
17. Copy the push commands and store the credentials in the Gitlab CI/CD settings
18. Copy my `.gitlab-ci.yml` file into the repository and push the code to Gitlab
19. Check the Gitlab CI/CD pipeline to see if the Docker image was pushed to the ECR Registry
20. Go to the AWS Lambda console and create a new Lambda Function using the ECR image
21. Add environment variables for the Qdrant and Cohere credentials in the Lambda Function settings
22. Add an API Gateway trigger to the Lambda Function
23. Test the Lambda Function using the API Gateway URL with the following URL: `https://wqkuzp82v4.execute-api.us-east-1.amazonaws.com/default/mini7?q='mobile'`

## References
1. https://github.com/qdrant/rust-client
2. https://gitlab.com/jeremymtan/jeremytan_ids721_week7/-/tree/main?ref_type=heads
3. https://gitlab.com/dukeaiml/IDS721/ids-721-mini-proj-7